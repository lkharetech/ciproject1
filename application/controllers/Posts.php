<?php
class Posts extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('post');
    }
    public function index() {
        $data = array();

        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        // $this->load->model('post');
        $data['posts'] = $this->post->getRows();
        //$data['title'] = 'Post Archive';
        // print_r($posts);die;
        
        //load the list page view
        $this->load->view('view_posts', $data);
    }
    // Insert Method
    public function add() {
        // print_r("Hello");die;
        $data = array();
        $postData = array();

        if ($this->input->post('postSubmit')) {
            $this->form_validation->set_rules('title', 'post title', 'required');
            $this->form_validation->set_rules('content', 'post content', 'required');

            // $postData = array(
            //     'title' => $this->input->post('title'),
            //     'content' => $this->input->post('content')
            // );
            // print_r($_POST);die;
            $postData['title'] = $_POST['title'];
            $postData['content'] = $_POST['content'];

            // print_r($postData);die;

            if ($this->form_validation->run() == true) {
                // $this->load->model('post');
                $insert = $this->post->insert($postData);

                if ($insert) {
                    $this->session->set_userdata('success_msg', 'Post has been added successfully.');
                    redirect('posts');
                } else {
                    $data['error_msg'] = 'Some problems occurred during insertion, please try again.';
                }
            }
        }
        $data['post'] = $postData;
        // $data['title'] = 'Create Post';
        $data['action'] = 'ADD';
        // Load the add page view
        $this->load->view('view_add_edit',$data);
    }
    // View Single Post Method
    public function view($id) {
        $data = array();
        //check whether post id is not empty
        if(!empty($id)){
            $data['post'] = $this->post->getRows($id);
            //$data['title'] = $data['post']['title'];
            // print_r($data);die;
            //load the details page view
            $this->load->view('view_file', $data);
        } else {
            redirect('/posts');
        }
    }
    // Edit Method
    public function edit($id) {
        $data = array();
        
        //get post data
        $postData = $this->post->getRows($id);
        
        //if update request is submitted
        if($this->input->post('postSubmit')){
            //form field validation rules
            $this->form_validation->set_rules('title', 'post title', 'required');
            $this->form_validation->set_rules('content', 'post content', 'required');
            
            //prepare cms page data
            $postData = array(
                'title' => $this->input->post('title'),
                'content' => $this->input->post('content')
            );
            // print_r($postData);die;
            //validate submitted form data
            if($this->form_validation->run() == true){
                //update post data
                $update = $this->post->update($postData, $id);

                if($update) {
                    $this->session->set_userdata('success_msg', 'Post has been updated successfully.');
                    redirect('/posts');
                } else {
                    $data['error_msg'] = 'Some problems occurred, please try again.';
                }
            }
        }

        $data['post'] = $postData;
        $data['action'] = 'EDIT';
        
        //load the edit page view     
        $this->load->view('view_add_edit', $data);
    }
    // Delete Method
    public function delete($id) {
        //check whether post id is not empty
        if($id) {
            //delete post
            // print_r($post);die;
            $delete = $this->post->delete($id);
            
            if($delete) {
                $this->session->set_userdata('success_msg', 'Post has been removed successfully.');
            } else {
                $this->session->set_userdata('error_msg', 'Some problems occurred while deleting, please try again.');
            }
        }
        redirect('/posts');
    }
}
