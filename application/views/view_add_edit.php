<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>

<body>
    <!-- <br> -->
    <div class="container">
        <h2 style="text-align: center;">Add / Edit Post</h2>
        <div class="col-xs-12">
            <?php
            if (!empty($success_msg)) {
                echo '<div class="alert alert-success">' . $success_msg . '</div>';
            } elseif (!empty($error_msg)) {
                echo '<div class="alert alert-danger">' . $error_msg . '</div>';
            }
            ?>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><?php echo $action; ?> POST
                        <a href="<?php echo base_url(); ?>posts" class="glyphicon glyphicon-arrow-left pull-right"></a>
                    </div>
                    <div class="panel-body">
                        <form action="" method="post" class="form">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" placeholder="Enter the title" value="<?php echo !empty($post['title']) ? $post['title'] : ''; ?>">
                                <?php echo form_error('title', '<p class="text-danger">', '</p>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="content">Content</label>
                                <textarea name="content" class="form-control" placeholder="Enter post content" id="" cols="30" rows="7"><?php echo !empty($post['content']) ? $post['content'] : ''; ?></textarea>
                                <?php echo form_error('content', '<p class="text-danger">', '</p>'); ?>
                            </div>
                            <input type="submit" value="Submit" name="postSubmit" class="btn btn-primary">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>