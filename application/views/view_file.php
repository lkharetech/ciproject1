<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
    <h2 style="text-align: center;">View Page</h2>
    <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">Post Details <a href="<?php echo site_url('posts/'); ?>" class="glyphicon glyphicon-arrow-left pull-right"></a></div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Title:</label>
                        <p><?php echo !empty($post['title']) ? $post['title'] : ''; ?></p>
                    </div>
                    <div class="form-group">
                        <label>Content:</label>
                        <p><?php echo !empty($post['content']) ? $post['content'] : ''; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>