<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <h2 style="text-align: center;">CRUD WITH CI 3</h2>
        <?php if (!empty($success_msg)) { ?>
            <div class="col-xs-12">
                <div class="alert alert-success"><?php echo $success_msg; ?></div>
            </div>
        <?php } else if (!empty($error_msg)) { ?>
            <div class="col-xs-12">
                <div class="alert alert-danger"><?php echo $error_msg; ?></div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Posts
                        <a href="<?php echo base_url(); ?>posts/add" class="glyphicon glyphicon-plus pull-right"></a>
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width=10%>ID</th>
                                <th width=25%>TITLE</th>
                                <th width=40%>CONTENT</th>
                                <th width=25%>ACTION</th>
                            </tr>
                        </thead>
                        <tbody id="userData">
                            <?php if (!empty($posts)) : foreach ($posts as $post) : ?>
                                    <tr>
                                        <td><?php echo '#' . $post['id']; ?></td>
                                        <td><?php echo $post['title']; ?></td>
                                        <td><?php echo (strlen($post['content']) > 150) ? substr($post['content'], 0, 150) . '...' : $post['content']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('posts/view/' . $post['id']); ?>" class="glyphicon glyphicon-eye-open"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="<?php echo site_url('posts/edit/' . $post['id']); ?>" class="glyphicon glyphicon-edit"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="<?php echo site_url('posts/delete/' . $post['id']); ?>" class="glyphicon glyphicon-trash" onclick="return confirm('Are you sure to delete?')"></a>
                                        </td>
                                    </tr>
                                <?php endforeach;
                            else : ?>
                                <tr>
                                    <td colspan="4">Post(s) not found......</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</body>

</html>